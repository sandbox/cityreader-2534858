core = 7.x
api = 2

libraries[commerce_braintree][type] = "libraries"
libraries[commerce_braintree][download][type] = "file"
libraries[commerce_braintree][download][url] = "https://github.com/braintree/braintree_php/archive/3.2.0.zip"
libraries[commerce_braintree][directory_name] = "braintree_php"
libraries[commerce_braintree][destination] = "libraries"
