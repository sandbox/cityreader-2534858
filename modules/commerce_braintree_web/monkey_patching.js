var braintreeListener = function() {
  this.originalAddEventListener = Element.prototype.addEventListener;
  this.listeners = [];

  var self = this;

  Element.prototype.addEventListener = function (event, listener) {
    arguments = Array.prototype.slice.apply(arguments);

    self.originalAddEventListener.apply(this, arguments);

    arguments.unshift(this)

    //console.log(arguments);
    self.listeners.push(arguments);
  };

  return this;
}();

braintreeListener.undelegate = function (html) {
  arguments = Array.prototype.slice.apply(arguments);

  if (typeof html == 'string') {
    html = document.querySelector(html);
  }

  for (var key in this.listeners) {
    if (html == this.listeners[key][0]) {
      html.removeEventListener(this.listeners[key][1], this.listeners[key][2]);
    }
  }
}

