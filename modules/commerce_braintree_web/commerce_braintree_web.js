(function($) {

  Drupal.behaviors.commerceBraintreeWebChangePaymentMethod = {
    attach: function(context, settings) {
      var $paymentMethod = $('input[name="commerce_payment[payment_method]"]', context);
      var $reloadBtn = $('#reload-btn', context);

      if ($paymentMethod.length > 0 && $reloadBtn.length > 0) {
        var formId = settings.commerceBraintree.formId;
        var $form = $('#' + formId);

        $paymentMethod.bind('change', function(event) {
          $('.checkout-processing', this.$form).removeClass('element-invisible');

          // Remove submit listener of Braintree.js
          braintreeListener.undelegate($form[0]);

          // Click reload button to submit form.
          // Bind click event with form submit, as in Bootstrap theme,
          // clicking reload button does not submit form by default.
          $reloadBtn.click();
        });

        setTimeout(function () {
          $reloadBtn.unbind();
        }, 10);
      }
    }
  }

  Drupal.behaviors.commerceBraintreeWeb = {
    attach: function(context, settings) {
      if (context != document) {
        return;
      }

      if (typeof settings.commerceBraintree != 'undefined' && typeof braintree != 'undefined') {
        var $form = $('#' + settings.commerceBraintree.formId);
        Drupal.myCommerceBraintree = new Drupal.commerceBraintree($form, settings.commerceBraintree);
        Drupal.myCommerceBraintree.bootstrap();
      }

      // Braintree hijacks all submit buttons for this form. Simulate the back
      // button to make sure back submit still works.
      $('.checkout-cancel, .checkout-back', context).click(function(e) {
        e.preventDefault();
        window.history.back();
      });

    },

    detach: function (context, settings) {

    }
  }

  Drupal.commerceBraintree = function($form, settings) {
    this.settings = settings;
    this.$form = $form;
    this.fromId = this.$form.attr('id');
    this.$submit = this.$form.find('#edit-continue');

    return this;
  }

  Drupal.commerceBraintree.prototype.bootstrap = function() {
    var options = this.getOptions(this.settings.integration);

    braintree.setup(this.settings.clientToken, this.settings.integration, options);

    if (this.settings.integration == 'paypal') {
      this.bootstrapPaypal();
    }
  }

  Drupal.commerceBraintree.prototype.bootstrapPaypal = function() {
    this.$submit.attr('disabled', 'disabled');
  }

  Drupal.commerceBraintree.prototype.resetSubmitBtn = function() {
    $('.checkout-processing', this.$form).addClass('element-invisible');

    // Remove disbabled attribute added by commerce_checkout.js
    this.$submit.next('.checkout-continue').removeAttr('disabled');

    // Reset submit button.
    if (Drupal.behaviors.hideSubmitBlockit) {
      $.event.trigger('clientsideValidationFormHasErrors', this.$form);
    }
  }

  Drupal.commerceBraintree.prototype.jsValidateErrorHandler = function(response) {
    var message = this.errorMsg(response);
    this.showError(message);
  }

  Drupal.commerceBraintree.prototype.errorMsg = function(response) {
    var message;

    switch (response.message) {
      case 'User did not enter a payment method':
        message = Drupal.t('Please enter your credit card details.');
        break;

      case 'Some payment method input fields are invalid.':
        var fieldName = '';
        var fields = [];
        var invalidFields = this.$form.find('.braintree-hosted-fields-invalid');

        function getFieldName(id) {
          return id.replace('-', ' ');
        }

        if (invalidFields.length > 0) {
          invalidFields.each(function(index) {
            var id = $(this).attr('id');

            fields.push(Drupal.t(getFieldName(id)));
          });

          if (fields.length > 1) {
            var last = fields.pop()
            fieldName = fields.join(', ');
            fieldName += ' and ' + Drupal.t(last);
            message = Drupal.t('The @field you entered are invalid', {'@field': fieldName});
          }
          else {
            fieldName = fields.pop();
            message = Drupal.t('The @field you entered is invalid', {'@field': fieldName});
          }

        }
        else {
          message = Drupal.t('The payment details you entered are invalid');
        }

        message += Drupal.t(', please check your details and try again.');

        break;

      default:
        message = response.message;
    }

    return message;
  }

  Drupal.commerceBraintree.prototype.showError = function(message) {
    this.resetSubmitBtn();

    if (Drupal.myClientsideValidation) {
      if (typeof this.validator == 'undefined') {
        this.validator = Drupal.myClientsideValidation.validators[this.fromId];
      }

      var errors = {
        'commerce_payment[payment_details][braintree][errors]' : message
      };

      this.validator.showErrors(errors);
    }
  }

  Drupal.commerceBraintree.prototype.getOptions = function(integration) {
    var self = this;

    var options = {
      onError: $.proxy(self.onError, self),
    }

    var getCustomOptions = function() {
      options.id = self.fromId;
      options.hostedFields = {};

      // Set up hosted fields selector
      options.hostedFields = $.extend(options.hostedFields, self.settings.hostedFields);
      options.hostedFields.styles = self.settings.fieldsStyles;
      options.hostedFields.onFieldEvent = self.onFieldEvent;

      return options;
    }

    var getPayPalOptions = function() {
      options.container = self.settings.paypalContainer;
      options.onPaymentMethodReceived = $.proxy(self.onPaymentMethodReceived, self);
      return options;
    }

    if (integration == 'paypal') {
      options = getPayPalOptions();
    }
    else {
      options = getCustomOptions();
    }

    return options;
  }

  // Global event callback

  Drupal.commerceBraintree.prototype.onError = function (response) {
    if (response.type == 'VALIDATION') {
      this.jsValidateErrorHandler(response);
    }
    else {
      console.log('Other error', arguments);
    }
  }

  // Hosted Fields event callback

  /**
   * You can subscribe to events using the onFieldEvent callback. This
   * allows you to hook into focus, blur, and fieldStateChange.
   *
   * @param event
   *
   * @see https://developers.braintreepayments.com/javascript+php/guides/hosted-fields/events
   */
  Drupal.commerceBraintree.prototype.onFieldEvent = function (event) {
    //if (event.type === "focus") {
    //  // Handle focus
    //} else if (event.type === "blur") {
    //  // Handle blur
    //} else if (event.type === "fieldStateChange") {
    //  // Handle a change in validation or card type
    //  console.log(event.isValid); // true|false
    //  if (event.card) {
    //    console.log(event.card.type);
    //    // visa|master-card|american-express|diners-club|discover|jcb|unionpay|maestro
    //  }
    //}
  }

  // PayPal event callback

  /**
   * A successful completion of the PayPal flow by your customer will result in
   * certain information being returned to you, depending on the client
   * options you have set.
   *
   * @param response
   *
   * @see https://developers.braintreepayments.com/javascript+php/guides/paypal/client-side
   */
  Drupal.commerceBraintree.prototype.onPaymentMethodReceived = function (obj) {
    this.$submit.removeAttr('disabled');
  }

})(jQuery);
